#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <signal.h>
#include <time.h>

void bash_command(char *command, char *argv[]) {
    pid_t child = fork();
    int status;

    if (child < 0) {
		printf("Error: fork failed!\n");
		exit(EXIT_FAILURE);
	} else if (child > 1) {
		wait(&status);
	} else if (child == 0) {
		execv(command, argv);
		exit(EXIT_SUCCESS);
	}
}

int convertValue(char *argv) {
	if (!strcmp(argv, "*") || !strcmp(argv, "0")) {
		return 0;
	} else if (atoi(argv) == 0 && strcmp(argv, "0")) {
		return -1;
	} else {
		return atoi(argv);
	}
}

int checkValue(int hour, int minute, int second) {
	if (hour < 0 || hour > 23) {
		return 1;
	} else if (minute < 0 || minute > 59) {
		return 1;
	} else if (second < 0 || second > 59) {
		return 1;
	} else {
		return 0;
	}
}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        printf("Error: invalid arguments\n");
        printf("Usage: %s [hour|*] [minute|*] [second|*] [path]\n", argv[0]);
		return 0;
    }

	int hour = convertValue(argv[1]);
	int minute = convertValue(argv[2]);
	int second = convertValue(argv[3]);
	char *fileExtension = strrchr(argv[4], '.');

	if (checkValue(hour, minute, second) || strcmp(fileExtension, ".sh")) {
        printf("Error: invalid arguments\n");
        printf("Usage: %s [hour|*] [minute|*] [second|*] [path]\n", argv[0]);
		return 0;
	} 

	pid_t sid, pid = fork();

	if (pid < 0) {
		printf("Process can't be created\n");
		exit(1);
	} else if (pid > 0) {
		exit(0);
	}

	// child process starts here
	sid = setsid();
	umask(0);

	while (1) {
		time_t now = time(NULL);
		struct tm *local_time = localtime(&now);

		int hour_now = local_time->tm_hour;
		int minute_now = local_time->tm_min;
		int second_now = local_time->tm_sec;

		if (hour_now == hour && minute_now == minute && second_now == second) {
			char *execute[] = {"bash", argv[4], "&", NULL};
			bash_command("/usr/bin/bash", execute);
		}

		sleep(1);
	}

    return 0;
}
