#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include <wait.h>
#include <string.h>

void bash_command(char *command, char *argv[]);
void generateKiller(int signalValue, char *mode);
void downloadPics(char *dir);

void bash_command(char *command, char *argv[]) {
	pid_t child = fork();
	int status;

	if (child < 0) {
		printf("Error: fork failed!\n");
		exit(EXIT_FAILURE);
	} else if (child > 1) {
		wait(&status);
	} else if (child == 0) {
		execv(command, argv);
		exit(EXIT_SUCCESS);
	}
}

void generateKiller(int processID, char *mode) {
	chdir("..");
	FILE *fp;
	fp = fopen("killer.c", "w");
	fputs("#include <stdio.h>\n", fp);
	fputs("#include <signal.h>\n", fp);
	fputs("#include <sys/types.h>\n\n", fp);
	fputs("int main() {\n", fp);
	if (mode == "-a") fprintf(fp, "\tkill(-%d, SIGKILL);\n", processID);
	else fprintf(fp, "\tkill(%d, SIGTERM);\n", processID);
	fputs("\tremove(\"killer\");\n", fp);
	fputs("}\n", fp);
	fclose(fp);
	char *argv[] = { "gcc", "killer.c", "-o", "killer", NULL };
	char *removeFile[] = { "rm", "killer.c", NULL };
	bash_command("/usr/bin/gcc", argv);
	bash_command("/usr/bin/rm", removeFile);
	chdir("folder_khusus");
}


void downloadPics(char *dir) {
	pid_t child = fork();
	int status;

	if (child < 0) {
		printf("Error: downloadPics fork failed!\n");
		exit(EXIT_FAILURE);
	} else if (child == 0) {
		chdir(dir);

		// main function
		for (int i = 1; i <= 15; i++) {
			char link[30] = "https://picsum.photos/";
			char size[5];
			char picsName[50];
			char workDir[100];

			time_t rawtime, seconds;
			struct tm *info;

			DIR *currentWorkDir = opendir(getcwd(workDir, sizeof(workDir)));
			struct dirent *entry;

			time(&rawtime);
			info = localtime(&rawtime);
			strftime(picsName, sizeof(picsName), "%Y-%m-%d_%H:%M:%S.jpg", info);
			snprintf(size, sizeof(size), "%ld", ((rawtime % 1000) + 50));
			strncat(link, size, 5);

			char *argv[] = { "wget", "-q", "--no-check-certificate", link, "-O", picsName, NULL };
			bash_command("/usr/bin/wget", argv);

			if (i < 15) sleep(5);
		}

		char zipname[50];
		snprintf(zipname, sizeof(zipname), "%s.zip", dir);

		chdir("..");
		char *zip_argv[] = { "zip", "-qr", zipname, dir, NULL };
		char *rm_dir[] = { "rm", "-rf", dir, NULL };
		bash_command("/usr/bin/zip", zip_argv);
		bash_command("/usr/bin/rm", rm_dir);

		exit(EXIT_SUCCESS);
	}
}

int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Jumlah argumen tidak memenuhi\n");
        printf("Pastikan untuk menjalankan program ini dengan argumen -a atau -b\n");
		exit(EXIT_SUCCESS);
	} else {
		if (strcmp(argv[1], "-a") && strcmp(argv[1], "-b")) {
			printf("Argumen invalid\n");
        	printf("Pastikan untuk menjalankan program ini dengan argumen -a atau -b\n");
			exit(EXIT_SUCCESS);
		} else {
			DIR *dir = opendir("folder_khusus");
			if (!dir) {
				mkdir("folder_khusus", 0777);
			}
			closedir(dir);

			chdir("folder_khusus");

			pid_t pid, sid;
			pid = fork();

			if (pid < 0) {
				exit(EXIT_FAILURE);
			} else if (pid > 0) {
				exit(EXIT_SUCCESS);
			}

			sid = setsid();
			umask(0);

			if (!strcmp(argv[1], "-a")) generateKiller(sid, "-a");
			else generateKiller(sid, "-b");

			while (1) {
				time_t rawtime;
				struct tm *info;

				char buffer[80];

				time(&rawtime);
				info = localtime(&rawtime);

				strftime(buffer, sizeof(buffer), "%Y-%m-%d_%H:%M:%S", info);
				mkdir(buffer, 0777);

				downloadPics(buffer);

				sleep(30);
			}
		}
	}
}
