#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <wait.h>

char* link_drive = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
char* nama_zip = "binatang.zip";

void bash_command(char* argv[], char* command_execute) {
  pid_t id_child = fork();
  int status;

  if (id_child < 0) {
    printf("Error: Fork Failed\n");
    exit(1);
  }
  else if (id_child == 0) {
    // printf("Child Spawned\n");
    execv(command_execute, argv);
    exit(EXIT_SUCCESS);
  }
  else {
    wait(&status);
  }
}

void move_file(char* command_execute, char* classifier, char* class_directory) {
  pid_t id_child = fork();
  int status;

  if (id_child < 0) {
    printf("Error: Fork Failed\n");
    exit(1);
  }
  else if (id_child == 0) {
    // printf("Child Spawned\n");
    execlp(command_execute, "find", "-type", "f", "-name", classifier, "-exec", "mv", "-t", class_directory, "{}", "+", NULL);
    exit(EXIT_SUCCESS);
  }
  else {
    wait(&status);
  }
}

int main() {
  // "-d", "tes/",
  char* download_argv[] = { "wget","-q", "--no-check-certificate", link_drive, "-O", nama_zip, NULL };
  bash_command(download_argv, "/bin/wget");
  char* unzip_argv[] = { "unzip", "-n", "-q", nama_zip,  NULL };
  bash_command(unzip_argv, "/bin/unzip");

  // "-exec", "echo", "-n", "Hewan yang dijaga : ", "{}", "+",
  char* echo_argv[] = { "echo", "-n", "Hewan yang dijaga : ", NULL };
  bash_command(echo_argv, "/bin/echo");
  char* shuf_argv[] = { "find", "-type", "f", "-name", "*.jpg",  "-exec", "shuf", "-n1", "-e", "{}", "+", NULL };
  bash_command(shuf_argv, "/bin/find");

  char* mkdir_argv[] = { "mkdir", "-p", "HewanDarat", "HewanAmphibi", "HewanAir", NULL };
  bash_command(mkdir_argv, "/bin/mkdir");

  move_file("/bin/find", "*_darat.jpg", "HewanDarat");
  move_file("/bin/find", "*_air.jpg", "HewanAir");
  move_file("/bin/find", "*_amphibi.jpg", "HewanAmphibi");

  char* zip_darat[] = { "zip", "-r", "-1", "-9", "-q", "HewanDarat.zip", "HewanDarat", NULL };
  bash_command(zip_darat, "/bin/zip");
  char* zip_air[] = { "zip", "-r", "-1", "-9", "-q", "HewanAir.zip", "HewanAir", NULL };
  bash_command(zip_air, "/bin/zip");
  char* zip_amphibi[] = { "zip", "-r", "-1", "-9", "-q", "HewanAmphibi.zip", "HewanAmphibi", NULL };
  bash_command(zip_amphibi, "/bin/zip");

  char* remove_dir[] = { "rm", "-rf", "HewanDarat", "HewanAir", "HewanAmphibi", NULL };
  bash_command(remove_dir, "/bin/rm");
}