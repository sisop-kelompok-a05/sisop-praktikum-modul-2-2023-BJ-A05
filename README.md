# sisop-praktikum-modul-2-2023-BJ-A05

## Anggota Kelompok

|     | Nama                      | NRP        |
| --- | ------------------------- | ---------- |
| 1   | SATRIA SULTHAN SABILILLAH | 5025201267 |
| 2   | Daud Dhiya' Rozaan        | 5025211021 |
| 3   | Urdhanaka Aptanagi        | 5025211123 |

## Soal 1

Pada program untuk jawaban no 1, sebagian besar menggunakan fungsi `bash_command` dibawah untuk membuat child process dan meng-_execute_ command bash yang diberikan. setiap `argv` yang ada di dalam program ini akan dipassing ke fungsi `bash_command` tersebut untuk di-_execute_

```c
void bash_command(char* argv[], char* command_execute) {
  pid_t id_child = fork();
  int status;

  if (id_child < 0) {
    printf("Error: Fork Failed\n");
    exit(1);
  }
  else if (id_child == 0) {
    execv(command_execute, argv);
    exit(EXIT_SUCCESS);
  }
  else wait(&status);
}
```

### 1a. Mengunduh dan meng-_extract_ zip-file dari suatu link drive

```c
char* download_argv[] = { "wget","-q", "--no-check-certificate", link_drive, "-O", nama_zip, NULL };
char* unzip_argv[] = { "unzip", "-n", "-q", nama_zip,  NULL };
```

menggunakan command `wget` untuk mendownload zip file dan `unzip` untuk meng-_extract_ zip-file.

### 1b. Get random file dari hasil extract zip file tadi, di directory saat ini

```c
char* shuf_argv[] = { "find", "-type", "f", "-name", "*.jpg",  "-exec", "shuf", "-n1", "-e", "{}", "+", NULL };
```

menggunakan command `find` untuk memfilter nama file yang berakhiran dengan extension `.jpg`, lalu dilanjutkan dengan command `shuf -n1` untuk mengambil 1 random file

### 1c. Melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya

```c
char* mkdir_argv[] = { "mkdir", "-p", "HewanDarat", "HewanAmphibi", "HewanAir", NULL };
```

pertama-tama, kita membuat beberapa directory (menggunakan command `mkdir`) untuk mengklasifikasi nama file sesuai regex nya (a.k.a klasifikasi hewan sesuai tempat tinggalnya)

```c
execlp("/bin/find", "find", "-type", "f", "-name", "*_darat.jpg", "-exec", "mv", "-t", "HewanDarat", "{}", "+", NULL);
```

untuk mengklasifikasi hewan sesuai tempat tinggalnya, kita dapat menggunakan syntax seperti contoh di atas. Kita menggunakan command `find` untuk mencari/memfilter nama file sesuai regex yang kita inginkan, dan dilanjutkan dengan command `mv` untuk memindahkan file-file yang telah kita filter ke directory yang sesuai.

📝 Note : syntax tersebut terletak di fungsi `move_file`, dan akan digunakan menyesuaikan regex nama file dan directory yang sesuai.

### 1d. Melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan

```c
char* zip_darat[] = { "zip", "-r", "-1", "-9", "-q", "HewanDarat.zip", "HewanDarat", NULL };

char* remove_dir[] = { "rm", "-rf", "HewanDarat", "HewanAir", "HewanAmphibi", NULL };
```

menggunakan command `zip` yang diikuti dengan beberapa parameter additional seperti `-1, -9, -q` (_type_ `man zip` _in ur terminal for complete documentation_) untuk men-zip direktori-direktori yang diinginkan. Lalu dilanjutkan dengan menghapus direktori-direktori yang telah di-zip menggunakan command `rm`.

\*\* Kendala : agak kesusahan di awal untuk get random file nya yang akhirnya terselesaikan menggunakan command `find + shuf`

#### Hasil :

- Hasil output

  <img width="250" src="img/soal1_hasil.png">

- Struktur Directory

  <img width="250" src="img/soal1_strukturAkhir.png">

- Isi dari file zip

  <img width="250" src="img/soal1_isiZip.png">

## Soal 2

Soal 2 meminta program untuk membuat folder dengan jeda 30 detik dengan nama folder timestamp waktu pembuatan folder tersebut. Kemudian di tiap-tiap folder diisi dengan gambar yang didownload dari website [picsum.photos](https://picsum.photos/) tiap 5 detik dan tiap gambar yang didownload berbentuk persegi dengan ukuran `(t % 1000) + 5` dengan t adalah detik Epoch UNIX dan nama gambar tersebut adalah timestamp download gambar.

```c
void bash_command(char *command, char *argv[]) {
  pid_t child = fork();
  int status;

  if (child < 0) {
    printf("Error: fork failed!\n");
    exit(EXIT_FAILURE);
  } else if (child > 1) {
    wait(&status);
  } else if (child == 0) {
    execv(command, argv);
    exit(EXIT_SUCCESS);
  }
}
```

Fungsi diatas digunakan untuk menjalankan perintah shell di program C yang akan digunakan karena di dalam program C akan menggunakan beberapa perintah shell.

```c
int main(int argc, char **argv) {
  if (argc != 2) {
    printf("Jumlah argumen tidak memenuhi\n");
    printf("Pastikan untuk menjalankan program ini dengan argumen -a atau -b\n");
    exit(EXIT_SUCCESS);
  } else {
    if (strcmp(argv[1], "-a") && strcmp(argv[1], "-b")) {
      printf("Argumen invalid\n");
      printf("Pastikan untuk menjalankan program ini dengan argumen -a atau -b\n");
      exit(EXIT_SUCCESS);
    } else {
      DIR *dir = opendir("folder_khusus");
      if (!dir) {
        mkdir("folder_khusus", 0777);
      }
      closedir(dir);

      chdir("folder_khusus");

      pid_t pid, sid;
      pid = fork();

      if (pid < 0) {
        exit(EXIT_FAILURE);
      } else if (pid > 0) {
        exit(EXIT_SUCCESS);
      }

      sid = setsid();
      umask(0);

      if (!strcmp(argv[1], "-a")) generateKiller(sid, "-a");
      else generateKiller(sid, "-b");

      while (1) {
        time_t rawtime;
        struct tm *info;

        char buffer[80];

        time(&rawtime);
        info = localtime(&rawtime);

        strftime(buffer, sizeof(buffer), "%Y-%m-%d_%H:%M:%S", info);
        mkdir(buffer, 0777);

        downloadPics(buffer);

        sleep(30);
      }
    }
  }
}
```

Potongan kode di atas adalah main function yang digunakan dalam program. Main function ini menghandle argumen yang diberikan oleh user, pembuatan file killer sesuai argumen yang diberikan oleh user, pembuatan folder tiap 30 detik, dan pemanggilan function untuk mendownload gambar di tiap-tiap folder.

```c
void downloadPics(char *dir) {
  pid_t child = fork();
  int status;

  if (child < 0) {
    printf("Error: downloadPics fork failed!\n");
    exit(EXIT_FAILURE);
  } else if (child == 0) {
    chdir(dir);

    // main function
    for (int i = 1; i <= 15; i++) {
      char link[30] = "https://picsum.photos/";
      char size[5];
      char picsName[50];
      char workDir[100];

      time_t rawtime, seconds;
      struct tm *info;

      DIR *currentWorkDir = opendir(getcwd(workDir, sizeof(workDir)));
      struct dirent *entry;

      time(&rawtime);
      info = localtime(&rawtime);
      strftime(picsName, sizeof(picsName), "%Y-%m-%d_%H:%M:%S.jpg", info);
      snprintf(size, sizeof(size), "%ld", ((rawtime % 1000) + 50));
      strncat(link, size, 5);

      char *argv[] = { "wget", "-q", "--no-check-certificate", link, "-O", picsName, NULL };
      bash_command("/usr/bin/wget", argv);

      if (i < 15) sleep(5);
    }

    char zipname[50];
    snprintf(zipname, sizeof(zipname), "%s.zip", dir);

    chdir("..");
    char *zip_argv[] = { "zip", "-qr", zipname, dir, NULL };
    char *rm_dir[] = { "rm", "-rf", dir, NULL };
    bash_command("/usr/bin/zip", zip_argv);
    bash_command("/usr/bin/rm", rm_dir);

    exit(EXIT_SUCCESS);
  }
}
```

Potongan kode di atas adalah function yang meng-handle download gambar di tiap-tiap folder. Karena gambar yang dibutuhkan di tiap-tiap folder hanyalah 15 gambar, maka for loop yang dipakai adalah `for (int i = 0; i < 15; i++)`. Jika `i < 15` yang berarti jumlah gambar yang terdownload belum mencapai 15 gambar, maka for loop akan `sleep(5)` yang berarti for loop akan berhenti selama 5 detik kemudian akan menjalankan lagi kode pada body for loop. Jika `i = 15`, maka akan keluar dari for loop dan akan menjalankan proses zip folder yang telah terisi oleh 15 gambar.

```c
void generateKiller(int processID, char *mode) {
  chdir("..");
  FILE *fp;
  fp = fopen("killer.c", "w");
  fputs("#include <stdio.h>\n", fp);
  fputs("#include <signal.h>\n", fp);
  fputs("#include <sys/types.h>\n\n", fp);
  fputs("int main() {\n", fp);
  if (!strcmp(mode, "-a")) fprintf(fp, "\tkill(-%d, SIGKILL);\n", processID);
  else fprintf(fp, "\tkill(%d, SIGTERM);\n", processID);
  fputs("\tremove(\"killer\");\n", fp);
  fputs("}\n", fp);
  fclose(fp);
  char *argv[] = { "gcc", "killer.c", "-o", "killer", NULL };
  char *removeFile[] = { "rm", "killer.c", NULL };
  bash_command("/usr/bin/gcc", argv);
  bash_command("/usr/bin/rm", removeFile);
  chdir("folder_khusus");
}
```

Potongan kode di atas akan men-generate program killer untuk program utama. Mekanisme program killer ditentukan oleh argumen yang user berikan:

- Jika user menjalankan program dengan argumen `-a`, maka ketika program killer dijalankan, program utama akan langsung mengentikan semua operasinya. Untuk melakukan hal tersebut, proses kill yang dilakukan akan menggunakan `SIGKILL` dan negative PID dari program utama. Dengan begitu, ketika program killer dijalankan, program utama dan semua child process akan langsung berhenti.

- Jika user menjalankan program dengan argumen `-b`, maka ketika program killer dijalankan, program utama akan berhenti, namun tetap membiarkan proses download di setiap folder selesai. Untuk melakukan hal tersebut, proses kill yang digunakann akan menggunakan `SIGTERM` dan PID dari program utama. Dengan begitu, ketika program killer dijalankan, program utama berhenti namun child process masih berjalan sampai proses tersebut exit (yang berarti zip dan delete folder sudah dilakukan di folder tersebut).

\*\*Kendala yang dihadapi: membuat program killer yang sesuai dengan argumen yang diberikan oleh user. membuat daemon yang sesuai dengan keinginan user

## Soal 3

Pada program untuk jawaban no 3, sebagian besar menggunakan fungsi `bash_command` dibawah untuk membuat child process dan meng-_execute_ command bash yang diberikan. setiap `argv` yang ada di dalam program ini akan dipassing ke fungsi `bash_command` tersebut untuk di-_execute_

```c
void bash_command(char* argv[], char* command_execute) {
  pid_t id_child = fork();
  int status;

  if (id_child < 0) {
    printf("Error: Fork Failed\n");
    exit(1);
  }
  else if (id_child == 0) {
    execv(command_execute, argv);
    exit(EXIT_SUCCESS);
  }
  else wait(&status);
}
```

### 3a. Mengunduh dan meng-_extract_ zip-file dari suatu link drive, lalu menghapus zip-file nya

```c
char* download_argv[] = { "wget","-q", "--no-check-certificate", link_drive, "-O", nama_zip, NULL };
char* unzip_argv[] = { "unzip", "-n", "-q", nama_zip,  NULL };
char* remove_zip[] = { "rm", nama_zip, NULL };
```

menggunakan command `wget` untuk mendownload zip file, `unzip` untuk meng-_extract_ zip-file, dan `rm` untuk menghapus zip-file.

### 3b. Menghapus semua pemain yang bukan dari Manchester United

```c
chdir("players");
```

pertama-tama, kita pindah ke directory `players/` untuk mem-filter nama-nama file dari para pemain

```c
char* delete_non_MU[] = { "find", "-type", "f", "!", "-name", "*_ManUtd_*", "-delete", NULL };
```

lalu menggunakan command `find ! -name *_ManUtd_*` untuk menemukan semua pemain yang bukan dari ManUtd, lalu ditambahkan argumen `-delete` untuk menghapusnya

```c
chdir("..");
```

lalu kembali ke directory utama

### 3c. Mengkategorikan pemain sesuai dengan posisi mereka yaitu Kiper, Bek, Gelandang, dan Penyerang

```c
char* mkdir_argv[] = { "install", "-d", "Bek", "Penyerang", "Gelandang", "Kiper", NULL };
```

pertama-tama kita membuat folder-folder yang diminta menggunakan command `install -d`

```c
char* move_bek[] = { "find", "-type", "f", "-name", "*_Bek_*", "-exec", "mv", "-t", "Bek", "{}", "+", NULL };
```

lalu untuk mengkategorikan file-file yang diminta, akan menggunakan command `find` yang dilanjutkan dengan command `mv` untuk memindahkan ke folder yang sesuai. `argv` diatas akan menyesuaikan dengan regex nama file & nama folder sesuai kategori

### 3d. Membuat kesebelasan terbaik MU berdasarkan rating terbaik pemain, dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi `Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt` dan akan ditaruh di `/home/[users]/`

```c
void buatTim(int b, int g, int p)
```

menggunakan fungsi `buatTim` untuk membuat kesebelesan terbaik dengan input 3 value integer dengan urutan jumlah bek, jumlah gelandang, dan jumlah striker.

```c
snprintf(bek_argv, sizeof(bek_argv), "ls Bek | sort -nr -t _ -k 4 | head -n %d >> /home/daudhiyaa/Formasi_%d-%d-%d.txt", b, b, g, p);
char* get_bek[] = { "bash", "-c", bek_argv, NULL };
```

di dalam fungsi `buatTim`, akan ada generator string untuk dimasukkan ke dalam argumen `argv` menggunakan `snprintf`, dan disesuaikan sesuai kategori masing-masing (bek, gelandang, penyerang, kiper). setelah string tersebut dimasukkan ke dalam argumen `argv`, maka `argv` akan dipassing ke fungsi `bash_command` untuk di-_execute_

\*\* Kendala : agak kesusahan dalam membuat fungsi `buatTim()` terutama untuk customize `argv` nya, yang akhirnya terselesaikan menggunakan `snprintf`

#### Hasil :

- Struktur Directory

  <img width="250" src="img/soal3_strukturDirektori.png">

- Formasi kesebelasan

  <img width="250" src="img/soal3_formasi.png">

## Soal 4

Soal 4 meminta untuk membuatkan program C yang berfungsi seperti cron (menjalankan program shell di waktu yang telah ditentukan). Selain itu, program tersebut berjalan di background.

```c
void bash_command(char *command, char *argv[]) {
    pid_t child = fork();
    int status;

    if (child < 0) {
    printf("Error: fork failed!\n");
    exit(EXIT_FAILURE);
  } else if (child > 1) {
    wait(&status);
  } else if (child == 0) {
    execv(command, argv);
    exit(EXIT_SUCCESS);
  }
}
```

Potongan kode di atas akan membuat child process dan menjalankan perintah shell. Function ini akan digunakan untuk menjalankan program shell di main function.

```c
int convertValue(char *argv) {
  if (!strcmp(argv, "*") || !strcmp(argv, "0")) {
    return 0;
  } else if (atoi(argv) == 0 && strcmp(argv, "0")) {
    return -1;
  } else {
    return atoi(argv);
  }
}
```

Potongan kode di atas digunakan untuk meng-convert argumen waktu yang diberikan oleh user. Karena argumen yang diperbolehkan adalah angka dan bintang (\*), function ini akan me-return nilai 0 jika argumen yang diberikan oleh user adalah 0 atau \* (\* akan dianggap sebagai 0), nilai 1 jika argumen yang diberikan user bukanlah angka atau \*, atau nilai dari angka jika argumen yang diberikan oleh user adalah angka.

```c
int checkValue(int hour, int minute, int second) {
  if (hour < 0 || hour > 23) {
    return 1;
  } else if (minute < 0 || minute > 59) {
    return 1;
  } else if (second < 0 || second > 59) {
    return 1;
  } else {
    return 0;
  }
}
```

Potongan kode di atas adalah function untuk memeriksa apakah nilai dari `hour`, `minute`, dan `second` berada di range yang diperbolehkan.

```c
int main(int argc, char *argv[]) {
  if (argc != 5) {
    printf("Error: invalid arguments\n");
    printf("Usage: %s [hour|*] [minute|*] [second|*] [path]\n", argv[0]);
    return 0;
  }

  int hour = convertValue(argv[1]);
  int minute = convertValue(argv[2]);
  int second = convertValue(argv[3]);
  char *fileExtension = strrchr(argv[4], '.');

  if (checkValue(hour, minute, second) || strcmp(fileExtension, ".sh")) {
    printf("Error: invalid arguments\n");
    printf("Usage: %s [hour|*] [minute|*] [second|*] [path]\n", argv[0]);
    return 0;
  }

  pid_t sid, pid = fork();

  if (pid < 0) {
    printf("Process can't be created\n");
    exit(1);
  } else if (pid > 0) {
    exit(0);
  }

  // child process starts here
  sid = setsid();
  umask(0);

  while (1) {
    time_t now = time(NULL);
    struct tm *local_time = localtime(&now);

    int hour_now = local_time->tm_hour;
    int minute_now = local_time->tm_min;
    int second_now = local_time->tm_sec;

    if (hour_now == hour && minute_now == minute && second_now == second) {
      char *execute[] = {"bash", "-c", argv[4], "&", NULL};
      bash_command("/usr/bin/bash", execute);
    }

    sleep(1);
  }

  return 0;
}
```

Potongan kode di atas adalah main function dari program. Program utama ini akan memeriksa apakah argumen yang diberikan oleh user sudah sesuai apa belum. Kemudian jika argumen yang diberikan oleh user sudah sesuai, maka program utama akan membuat child process baru. Child process ini akan memeriksa waktu sekarang dengan waktu yang diberikan oleh user. Jika waktu sekarang dan waktu yang diberikan oleh user sama, maka program shell yang diberikan oleh user akan dijalankan. Untuk mengurangi CPU State dari child process, pemeriksaan waktu akan menggunakan `sleep(1)`.

\*\*Kendala: mengatur waktu di di cron