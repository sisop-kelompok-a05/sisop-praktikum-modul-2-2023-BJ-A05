#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <wait.h>
#include <string.h>

char* link_drive = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
char* nama_zip = "players.zip";

void bash_command(char* argv[], char* command_execute) {
  pid_t id_child = fork();
  int status;

  if (id_child < 0) {
    printf("Error: Fork Failed\n");
    exit(1);
  }
  else if (id_child == 0) {
    execv(command_execute, argv);
    exit(EXIT_SUCCESS);
  }
  else wait(&status);
}

void buatTim(int b, int g, int p) {
  char bek_argv[79], gelandang_argv[85], penyerang_argv[85], kiper_argv[85];
  char* username = getenv("USER");

  snprintf(bek_argv, sizeof(bek_argv), "ls Bek | sort -nr -t _ -k 4 | head -n %d >> /home/%s/Formasi_%d-%d-%d.txt", b, username, b, g, p);
  char* get_bek[] = { "bash", "-c", bek_argv, NULL };
  bash_command(get_bek, "/bin/bash");

  snprintf(gelandang_argv, sizeof(gelandang_argv), "ls Gelandang | sort -nr -t _ -k 4 | head -n %d >> /home/%s/Formasi_%d-%d-%d.txt", g, username, b, g, p);
  char* get_gelandang[] = { "bash", "-c", gelandang_argv, NULL };
  bash_command(get_gelandang, "/bin/bash");

  snprintf(penyerang_argv, sizeof(penyerang_argv), "ls Penyerang | sort -nr -t _ -k 4 | head -n %d >> /home/%s/Formasi_%d-%d-%d.txt", p, username, b, g, p);
  char* get_penyerang[] = { "bash", "-c", penyerang_argv, NULL };
  bash_command(get_penyerang, "/bin/bash");

  snprintf(kiper_argv, sizeof(kiper_argv), "ls Kiper | sort -nr -t _ -k 4 | head -n 1 >> /home/%s/Formasi_%d-%d-%d.txt", username, b, g, p);
  char* get_kiper[] = { "bash", "-c", kiper_argv, NULL };
  bash_command(get_kiper, "/bin/bash");
}

int main() {
  char* download_argv[] = { "wget","-q", "--no-check-certificate", link_drive, "-O", nama_zip, NULL };
  bash_command(download_argv, "/bin/wget");
  char* unzip_argv[] = { "unzip", "-n", "-q", nama_zip,  NULL };
  bash_command(unzip_argv, "/bin/unzip");
  char* remove_zip[] = { "rm", nama_zip, NULL };
  bash_command(remove_zip, "/bin/rm");

  chdir("players");
  char* delete_non_MU[] = { "find", "-type", "f", "!", "-name", "*_ManUtd_*", "-delete", NULL };
  bash_command(delete_non_MU, "/bin/find");
  chdir("..");

  char* mkdir_argv[] = { "install", "-d", "Bek", "Penyerang", "Gelandang", "Kiper", NULL };
  bash_command(mkdir_argv, "/bin/install");

  char* move_bek[] = { "find", "-type", "f", "-name", "*_Bek_*", "-exec", "mv", "-t", "Bek", "{}", "+", NULL };
  bash_command(move_bek, "/bin/find");
  char* move_gelandang[] = { "find", "-type", "f", "-name", "*_Gelandang_*", "-exec", "mv", "-t", "Gelandang", "{}", "+", NULL };
  bash_command(move_gelandang, "/bin/find");
  char* move_penyerang[] = { "find", "-type", "f", "-name", "*_Penyerang_*", "-exec", "mv", "-t", "Penyerang", "{}", "+", NULL };
  bash_command(move_penyerang, "/bin/find");
  char* move_kiper[] = { "find", "-type", "f", "-name", "*_Kiper_*", "-exec", "mv", "-t", "Kiper", "{}", "+", NULL };
  bash_command(move_kiper, "/bin/find");

  char* remove_dir[] = { "rm", "-r", "players", NULL };
  bash_command(remove_dir, "/bin/rm");

  buatTim(4, 3, 3);
}
